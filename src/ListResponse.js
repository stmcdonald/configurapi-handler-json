const JsonResponse = require('./JsonResponse');
const ErrorResponse = require('configurapi').ErrorResponse;

module.exports = class ListResponse extends JsonResponse
{
    constructor(collection, additionalProperties = {}, statusCode = 200, headers = {})
    {
        if(collection && collection.constructor !== Array)
        {
            throw new ErrorResponse("An array is expcted for ListResponse.", 500);
        }

        let total = collection ? collection.length : 0;
        let start = 0;
        
        super(Object.assign({start: start, total: total, items: collection}, additionalProperties), statusCode, headers);
    }
};